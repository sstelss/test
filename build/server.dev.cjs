/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./main/routes.js":
/*!************************!*\
  !*** ./main/routes.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ((components={})=>[{path:'/',exact:true,component:components.PHome}]);

/***/ }),

/***/ "./model/TestThingModel.js":
/*!*********************************!*\
  !*** ./model/TestThingModel.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TestThingModel; });
/* harmony import */ var startupjs_orm__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! startupjs/orm */ "./node_modules/startupjs/orm.js");
class TestThingModel extends startupjs_orm__WEBPACK_IMPORTED_MODULE_0__["BaseModel"]{async addSelf(){await this.root.add(this.getCollection(),{id:this.getId(),counter:0});}async reset(){await this.set('counter',0);}}

/***/ }),

/***/ "./model/index.js":
/*!************************!*\
  !*** ./model/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _TestThingModel__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./TestThingModel */ "./model/TestThingModel.js");
/* harmony default export */ __webpack_exports__["default"] = (function(racer){racer.orm('testThings.*',_TestThingModel__WEBPACK_IMPORTED_MODULE_0__["default"]);});

/***/ }),

/***/ "./node_modules/@startupjs/app/server/index.js":
/*!*****************************************************!*\
  !*** ./node_modules/@startupjs/app/server/index.js ***!
  \*****************************************************/
/*! exports provided: initApp, initUpdateApp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "initUpdateApp", function() { return initUpdateApp; });
/* harmony import */ var _initApp__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./initApp */ "./node_modules/@startupjs/app/server/initApp.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "initApp", function() { return _initApp__WEBPACK_IMPORTED_MODULE_0__["default"]; });

function initUpdateApp(){throw Error('[@startupjs/app] app critical version API has changed. '+'Please update it following the new readme from '+'https://github.com/startupjs/startupjs/tree/master/packages/app');}

/***/ }),

/***/ "./node_modules/@startupjs/app/server/initApp.js":
/*!*******************************************************!*\
  !*** ./node_modules/@startupjs/app/server/initApp.js ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return initApp; });
function initApp(ee,{criticalVersion}={}){if(criticalVersion){ee.on('middleware',expressApp=>{expressApp.use(function(req,res,next){req.model.set('_session.criticalVersion',criticalVersion);next();});});}ee.on('routes',expressApp=>{expressApp.get('/api/serverSession',function(req,res){return res.json(req.model.get('_session'));});});}

/***/ }),

/***/ "./node_modules/@startupjs/init/index.js":
/*!***********************************************!*\
  !*** ./node_modules/@startupjs/init/index.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _lib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./lib */ "./node_modules/@startupjs/init/lib/index.server.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _lib__WEBPACK_IMPORTED_MODULE_0__["default"]; });



/***/ }),

/***/ "./node_modules/@startupjs/init/lib/index.server.js":
/*!**********************************************************!*\
  !*** ./node_modules/@startupjs/init/lib/index.server.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _server__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./server */ "./node_modules/@startupjs/init/lib/server/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _server__WEBPACK_IMPORTED_MODULE_0__["default"]; });



/***/ }),

/***/ "./node_modules/@startupjs/init/lib/server/index.js":
/*!**********************************************************!*\
  !*** ./node_modules/@startupjs/init/lib/server/index.js ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var sharedb__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! sharedb */ "sharedb");
/* harmony import */ var sharedb__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(sharedb__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _util_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../util/common */ "./node_modules/@startupjs/init/lib/util/common.js");
/* harmony default export */ __webpack_exports__["default"] = (options=>{Object(_util_common__WEBPACK_IMPORTED_MODULE_1__["default"])(sharedb__WEBPACK_IMPORTED_MODULE_0___default.a,options);});

/***/ }),

/***/ "./node_modules/@startupjs/init/lib/util/batch.server.js":
/*!***************************************************************!*\
  !*** ./node_modules/@startupjs/init/lib/util/batch.server.js ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (fn=>fn());

/***/ }),

/***/ "./node_modules/@startupjs/init/lib/util/common.js":
/*!*********************************************************!*\
  !*** ./node_modules/@startupjs/init/lib/util/common.js ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var rich_text__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rich-text */ "rich-text");
/* harmony import */ var rich_text__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(rich_text__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var racer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! racer */ "racer");
/* harmony import */ var racer__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(racer__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var racer_lib_Model_RemoteDoc__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! racer/lib/Model/RemoteDoc */ "racer/lib/Model/RemoteDoc");
/* harmony import */ var racer_lib_Model_RemoteDoc__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(racer_lib_Model_RemoteDoc__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _batch__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./batch */ "./node_modules/@startupjs/init/lib/util/batch.server.js");
/* harmony import */ var _startupjs_orm__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @startupjs/orm */ "./node_modules/@startupjs/orm/lib/index.js");
/* harmony default export */ __webpack_exports__["default"] = ((ShareDB,{orm}={})=>{ShareDB.types.register(rich_text__WEBPACK_IMPORTED_MODULE_0___default.a.type);const oldRemoteDocOnOp=racer_lib_Model_RemoteDoc__WEBPACK_IMPORTED_MODULE_2___default.a.prototype._onOp;racer_lib_Model_RemoteDoc__WEBPACK_IMPORTED_MODULE_2___default.a.prototype._onOp=function(){if(this.shareDoc.type===rich_text__WEBPACK_IMPORTED_MODULE_0___default.a.type)return;return oldRemoteDocOnOp.apply(this,arguments);};racer__WEBPACK_IMPORTED_MODULE_1___default.a.Model.prototype.batch=_batch__WEBPACK_IMPORTED_MODULE_3__["default"];if(orm){racer__WEBPACK_IMPORTED_MODULE_1___default.a.use(_startupjs_orm__WEBPACK_IMPORTED_MODULE_4__["default"]);racer__WEBPACK_IMPORTED_MODULE_1___default.a.use(orm);}});

/***/ }),

/***/ "./node_modules/@startupjs/orm/lib/index.js":
/*!**************************************************!*\
  !*** ./node_modules/@startupjs/orm/lib/index.js ***!
  \**************************************************/
/*! exports provided: default, ChildModel, BaseModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ChildModel", function() { return ChildModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseModel", function() { return BaseModel; });
/* harmony import */ var racer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! racer */ "racer");
/* harmony import */ var racer__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(racer__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _promisifyRacer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./promisifyRacer */ "./node_modules/@startupjs/orm/lib/promisifyRacer.js");
const Model=racer__WEBPACK_IMPORTED_MODULE_0___default.a.Model;global.STARTUP_JS_ORM={};/* harmony default export */ __webpack_exports__["default"] = (function(racer){if(racer.orm)return;racer._orm=global.STARTUP_JS_ORM;racer.orm=function(pattern,OrmEntity,alias){var name=alias||pattern;if(global.STARTUP_JS_ORM[name])throw alreadyDefinedError(pattern,alias);global.STARTUP_JS_ORM[name]={pattern:pattern,regexp:patternToRegExp(pattern),OrmEntity:OrmEntity};};Model.prototype.at=function(subpath,alias){var path=this.path(subpath);return this.scope(path,alias);};Model.prototype._scope=function(path){var ChildModel=Model.ChildModel;var model=new ChildModel(this);model._at=path;return model;};Model.prototype.scope=function(path,alias){if(alias){if(global.STARTUP_JS_ORM[alias]){return this.__createScopedModel(path,global.STARTUP_JS_ORM[alias].OrmEntity);}else{throw new Error('Non-existent alias of the OrmEntity specified: '+alias+'\n\n'+'Most likely you have specified the path incorrectly in '+'".scope()" or ".at()"\n\n'+'The path must be passed as a single string separated by dots, '+'for example:\n\n'+'CORRECT:\n'+"$root.at('users.' + userId)\n\n"+'INCORRECT:\n'+"$root.at('users', userId)");}}var segments=this._dereference(this.__splitPath(path),true);var fullPath=segments.join('.');for(var name in global.STARTUP_JS_ORM){var regexp=global.STARTUP_JS_ORM[name].regexp;if(regexp.test(fullPath)){return this.__createScopedModel(path,global.STARTUP_JS_ORM[name].OrmEntity);}}return this._scope(path);};Model.prototype.__createScopedModel=function(path,OrmEntity){var model;if(OrmEntity.factory||OrmEntity.prototype.factory){model=OrmEntity(this._scope(path),this);if(!model)model=this._scope(path);}else{model=new OrmEntity(this);}model._at=path;return model;};Model.prototype.__splitPath=function(path){return path&&path.split('.')||[];};Object(_promisifyRacer__WEBPACK_IMPORTED_MODULE_1__["default"])();});function patternToRegExp(pattern){pattern=pattern.replace(/\$/g,'\\$').replace(/\./g,'\\.').replace(/\*/g,'([^\\.]*)');return new RegExp('^'+pattern+'$');}function alreadyDefinedError(pattern,alias){var msg;if(alias){msg="ORM entity with the alias '"+alias+"' is already defined. "+'Aliases must be unique. If you did already define the same ORM entity with '+"that alias name, just don't specify the alias at all -- path pattern is sufficient.";}else{msg="ORM entity matching the same path pattern '"+pattern+"' is already defined.";}return new Error(msg);}const ChildModel=Model.ChildModel;function BaseModel(){Model.ChildModel.apply(this,arguments);}BaseModel.prototype=Object.create(Model.ChildModel.prototype);BaseModel.prototype.constructor=BaseModel;BaseModel.prototype.getId=function(){var actualField=this.dereferenceSelf();return actualField.leaf();};BaseModel.prototype.getCollection=function(){var model=this.root;var actualField=this.dereferenceSelf();return model._splitPath(actualField.path())[0];};BaseModel.prototype.dereferenceSelf=function(){var model=this.root;var segments=model._splitPath(this.path());return model.scope(model._dereference(segments,true).join('.'));};

/***/ }),

/***/ "./node_modules/@startupjs/orm/lib/promisifyRacer.js":
/*!***********************************************************!*\
  !*** ./node_modules/@startupjs/orm/lib/promisifyRacer.js ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var racer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! racer */ "racer");
/* harmony import */ var racer__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(racer__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var racer_lib_Model_Query__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! racer/lib/Model/Query */ "racer/lib/Model/Query");
/* harmony import */ var racer_lib_Model_Query__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(racer_lib_Model_Query__WEBPACK_IMPORTED_MODULE_1__);
const Model=racer__WEBPACK_IMPORTED_MODULE_0___default.a.Model;const FIX_VALUE_CB={set:{minArgs:2},setNull:{minArgs:2},setEach:{minArgs:2},push:{minArgs:2},unshift:{minArgs:2},insert:{minArgs:3},remove:{minArgs:2,onlyValidate:true},move:{minArgs:3,onlyValidate:true},stringInsert:{minArgs:3,onlyValidate:true},stringRemove:{minArgs:3,onlyValidate:true},subtypeSubmit:{minArgs:3,onlyValidate:true},setDiff:{minArgs:2},setDiffDeep:{minArgs:2},setArrayDiff:{minArgs:2},setArrayDiffDeep:{minArgs:2}};const MUTATORS=['set','setNull','setEach','create','createNull','add','del','increment','push','unshift','insert','pop','shift','remove','move','stringInsert','stringRemove','subtypeSubmit','setDiff','setDiffDeep','setArrayDiff','setArrayDiffDeep'];const SUBSCRIPTIONS=['subscribe','fetch'];const ASYNC_METHODS=MUTATORS.concat(SUBSCRIPTIONS);/* harmony default export */ __webpack_exports__["default"] = (function(){for(const method in FIX_VALUE_CB){const{minArgs,onlyValidate}=FIX_VALUE_CB[method];Model.prototype[method]=fixValueCbApi(Model.prototype[method],method,minArgs,onlyValidate);}for(const method of ASYNC_METHODS){Model.prototype[method]=optionalPromisify(Model.prototype[method]);Model.prototype[method+'Async']=deprecationWarning(Model.prototype[method],method);}for(const method of SUBSCRIPTIONS){racer_lib_Model_Query__WEBPACK_IMPORTED_MODULE_1___default.a.prototype[method]=optionalPromisify(racer_lib_Model_Query__WEBPACK_IMPORTED_MODULE_1___default.a.prototype[method]);racer_lib_Model_Query__WEBPACK_IMPORTED_MODULE_1___default.a.prototype[method+'Async']=deprecationWarning(racer_lib_Model_Query__WEBPACK_IMPORTED_MODULE_1___default.a.prototype[method],method);}});function optionalPromisify(originalFn){return function optionalPromisifier(...args){if(typeof args[args.length-1]==='function'){return originalFn.apply(this,args);}else{return new Promise((resolve,reject)=>{args.push(function promisifyCallback(err,value){if(err)return reject(err);return resolve(value);});originalFn.apply(this,args);});}};}function fixValueCbApi(originalFn,methodName,minArgs,onlyValidate){return function(...args){if(typeof arguments[arguments.length-1]==='function'){if(arguments.length<minArgs){throw new Error('Not enough arguments for '+methodName);}else if(!onlyValidate&&arguments.length===minArgs){args.unshift('');}}return originalFn.apply(this,args);};}function deprecationWarning(originalFn,methodName){return function(){console.warn('model.'+methodName+'Async() is DEPRECATED and going to be '+'REMOVED soon!\n Please use '+methodName+'(), '+'it supports promises now and you can \'await\' it directly.');return originalFn.apply(this,arguments);};}

/***/ }),

/***/ "./node_modules/startupjs/app/server.js":
/*!**********************************************!*\
  !*** ./node_modules/startupjs/app/server.js ***!
  \**********************************************/
/*! exports provided: initApp, initUpdateApp */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _startupjs_app_server__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @startupjs/app/server */ "./node_modules/@startupjs/app/server/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "initApp", function() { return _startupjs_app_server__WEBPACK_IMPORTED_MODULE_0__["initApp"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "initUpdateApp", function() { return _startupjs_app_server__WEBPACK_IMPORTED_MODULE_0__["initUpdateApp"]; });



/***/ }),

/***/ "./node_modules/startupjs/init/index.js":
/*!**********************************************!*\
  !*** ./node_modules/startupjs/init/index.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _startupjs_init__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @startupjs/init */ "./node_modules/@startupjs/init/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _startupjs_init__WEBPACK_IMPORTED_MODULE_0__["default"]; });



/***/ }),

/***/ "./node_modules/startupjs/orm.js":
/*!***************************************!*\
  !*** ./node_modules/startupjs/orm.js ***!
  \***************************************/
/*! exports provided: default, ChildModel, BaseModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _startupjs_orm__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @startupjs/orm */ "./node_modules/@startupjs/orm/lib/index.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _startupjs_orm__WEBPACK_IMPORTED_MODULE_0__["default"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "ChildModel", function() { return _startupjs_orm__WEBPACK_IMPORTED_MODULE_0__["ChildModel"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "BaseModel", function() { return _startupjs_orm__WEBPACK_IMPORTED_MODULE_0__["BaseModel"]; });



/***/ }),

/***/ "./node_modules/startupjs/server.js":
/*!******************************************!*\
  !*** ./node_modules/startupjs/server.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _startupjs_server__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @startupjs/server */ "@startupjs/server");
/* harmony import */ var _startupjs_server__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_startupjs_server__WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (default from non-harmony) */ __webpack_require__.d(__webpack_exports__, "default", function() { return _startupjs_server__WEBPACK_IMPORTED_MODULE_0___default.a; });


/***/ }),

/***/ "./server.js":
/*!*******************!*\
  !*** ./server.js ***!
  \*******************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _server_index_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./server/index.js */ "./server/index.js");
Object(_server_index_js__WEBPACK_IMPORTED_MODULE_0__["default"])();

/***/ }),

/***/ "./server/api/index.js":
/*!*****************************!*\
  !*** ./server/api/index.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _testThing__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./testThing */ "./server/api/testThing.js");
const router=express__WEBPACK_IMPORTED_MODULE_0___default.a.Router();router.get('/test-thing',_testThing__WEBPACK_IMPORTED_MODULE_1__["default"]);/* harmony default export */ __webpack_exports__["default"] = (router);

/***/ }),

/***/ "./server/api/testThing.js":
/*!*********************************!*\
  !*** ./server/api/testThing.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (async(req,res)=>{const{model}=req;const $testThing=model.at('testThings.first');await $testThing.subscribe();res.json({name:'Test API',testThing:$testThing.get()});});

/***/ }),

/***/ "./server/index.js":
/*!*************************!*\
  !*** ./server/index.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return run; });
/* harmony import */ var startupjs_init__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! startupjs/init */ "./node_modules/startupjs/init/index.js");
/* harmony import */ var _model__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../model */ "./model/index.js");
/* harmony import */ var startupjs_server__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! startupjs/server */ "./node_modules/startupjs/server.js");
/* harmony import */ var _api__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./api */ "./server/api/index.js");
/* harmony import */ var _main_routes__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../main/routes */ "./main/routes.js");
/* harmony import */ var startupjs_app_server__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! startupjs/app/server */ "./node_modules/startupjs/app/server.js");
Object(startupjs_init__WEBPACK_IMPORTED_MODULE_0__["default"])({orm: _model__WEBPACK_IMPORTED_MODULE_1__["default"]});Object(startupjs_server__WEBPACK_IMPORTED_MODULE_2__["default"])({getHead,appRoutes:[...Object(_main_routes__WEBPACK_IMPORTED_MODULE_4__["default"])()]},(ee,options)=>{Object(startupjs_app_server__WEBPACK_IMPORTED_MODULE_5__["initApp"])(ee);ee.on('routes',expressApp=>{expressApp.use('/api',_api__WEBPACK_IMPORTED_MODULE_3__["default"]);});});function getHead(appName){return`
    <title>HelloWorld</title>
    <!-- Put vendor JS and CSS here -->
  `;}function run(){}

/***/ }),

/***/ 0:
/*!*****************************************!*\
  !*** multi @babel/polyfill ./server.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! @babel/polyfill */"@babel/polyfill");
module.exports = __webpack_require__(/*! ./server.js */"./server.js");


/***/ }),

/***/ "@babel/polyfill":
/*!**********************************!*\
  !*** external "@babel/polyfill" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@babel/polyfill");

/***/ }),

/***/ "@startupjs/server":
/*!************************************!*\
  !*** external "@startupjs/server" ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("@startupjs/server");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "racer":
/*!************************!*\
  !*** external "racer" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("racer");

/***/ }),

/***/ "racer/lib/Model/Query":
/*!****************************************!*\
  !*** external "racer/lib/Model/Query" ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("racer/lib/Model/Query");

/***/ }),

/***/ "racer/lib/Model/RemoteDoc":
/*!********************************************!*\
  !*** external "racer/lib/Model/RemoteDoc" ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("racer/lib/Model/RemoteDoc");

/***/ }),

/***/ "rich-text":
/*!****************************!*\
  !*** external "rich-text" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("rich-text");

/***/ }),

/***/ "sharedb":
/*!**************************!*\
  !*** external "sharedb" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sharedb");

/***/ })

/******/ });